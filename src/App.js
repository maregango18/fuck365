import React, { useState, useEffect } from 'react';
import Header from './components/header/Header';
import './App.css';
import Sidebar from './components/sidebar/Sidebar';
import Aside from './components/aside/Aside';
import Product from './components/catalog/Product';
import Catalog from './components/catalog/Catalog';

const App = () => {
  const [products, setProducts] = useState([]);
  const [sortType, setSortType] = useState('Sort By: New Arrivals');
  const [query, setQuery] = useState("");
  const [finalQuery, setFinalQuery] = useState();
  const [checkedProducts, setCheckedProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);

  // const [productsClone, setProductsClone] = useState([]);
  //const [checkedProducts, setCheckedProducts] = useState(false);

  // const handleOnChange = () => {
  //   const checked = 
  //   // const updatedCheckedProducts = checkedProducts.map((item, index) =>
  //   //   index === position ? !item : item
  //   );
  //   setCheckedProducts(updatedCheckedProducts);
  // };


  useEffect(() => {
    getProducts();
  }, []);


  useEffect(() => {
    const filterArray = products.filter((item) => {
      return item.title.toLowerCase().includes(finalQuery.toLowerCase())
    });
    setProducts(filterArray)
  }, [finalQuery]);


  const getProducts = async () => {
    const response = await fetch(
      "https://fakestoreapi.com/products"
    );
    const data = await response.json();
    setProducts(data);
    console.log(data);
  };

  useEffect(() => {
    const sortArray = type => {
      const types = {
        sortby: 'Sort By: New Arrivals',
        asc: 'Price: High To Low',
        desc: 'Price: Low To High',
        profH: 'Profit: High To Low',
        profL: 'Profit: Low To High',
      };
      let sorted = [];
      const sortProperty = types[type];
      if (sortProperty === "Sort By: New Arrivals") {
        sorted = [...products].sort((a, b) => (b.id > a.id) ? 1 : -1);
      } else if (sortProperty === "Price: High To Low") {
        sorted = [...products].sort((a, b) => b.price - a.price);
      } else if (sortProperty === "Price: Low To High") {
        sorted = [...products].sort((a, b) => a.price - b.price);
      } else if (sortProperty === "Profit: High To Low") {
        sorted = [...products].sort((a, b) => a.title.localeCompare(b.title));
      } else if (sortProperty === "Profit: Low To High") {
        sorted = [...products].sort((a, b) => b.title.localeCompare(a.title));
      }
      console.log(sorted);
      setProducts(sorted);
    };

    sortArray(sortType);
  },
    [sortType],
    [products],
    [setProducts]
  );

  const onChanged = (e) => setSortType(e.target.value);

  const changeQuery = (e) => {
    setQuery(e.target.value);
  }

  const getFinalQuery = (e) => {
    setFinalQuery(query);
    console.log(query);
  }

  const checkboxChanged = (e, product) => {
    if (checkedProducts.includes(product)) {
      setCheckedProducts([...checkedProducts.filter(prod => prod.id !== product.id)])
    } else {
      setCheckedProducts([...checkedProducts, product])
    }
  }

  const selectAll = () => {
    setCheckedProducts([...products])
  }

  const clearSelectedProducts = () => {
    setCheckedProducts([])
  }


  const chooseProduct = (product) => {
    setSelectedProduct(product);
  }

  //console.log(selectedProduct, 'checkedProducts')

  return (
    <div className="App">
      {selectedProduct && (
        <div className="modal" onClick={() => setSelectedProduct(null)}>
          <div className="modal__catalog__left">
            <div className="modal__catalog__title"> {selectedProduct.title}</div>
            <div class="modal__catalog__photo" >
              <img src={selectedProduct.image} alt=" " />
            </div>
            <div class="modal__catalog__prices"> {selectedProduct.price}$ </div>
          </div>
          <div className="modal__catalog__rigth">
            <p className="modal__catalog__name">Product Description:</p>
            <p className="modal__catalog__description">{selectedProduct.description}</p>
          </div>
        </div>
      )}

      <Sidebar />
      <div className="god">
        <Aside />
        <div className="Content">
          <Header selectAll={selectAll} clearSelectedProducts={clearSelectedProducts} query={query} onChange={onChanged} changeQuery={changeQuery} getFinalQuery={getFinalQuery} checkedProductsCount={checkedProducts.length} numberOfProducts={products.length} />
          <div className="catalog">
            <Catalog chooseProduct={chooseProduct} products={products} checkboxChanged={checkboxChanged} checkedProducts={checkedProducts} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
