import React, { useState } from 'react';
import './Header.css';
import Button from './Button';
import Search from './Search';
import Sort from './Sort';

const Header = ({ query, onChange, changeQuery, getFinalQuery, checkedProductsCount, numberOfProducts, selectAll, clearSelectedProducts }) => {
    // const [query, setQuery] = useState("");
    // const [finalText, setFinalText] = useState('men');

    // const changeQuery = (e) => {
    //     setQuery(e.target.value);
    // }
    // const getFinalText = (e) => {
    //     setFinalText(query);
    //     console.log(query);
    // }


    return (
        <>
            <header className={"header"}>
                <div className={"header__content"}>
                    {/* <Button content={"Select All"} /> */}
                    <div className="header__content">
                        <button className="header__selectAll" onClick={selectAll}>Select All</button>
                    </div>
                    <span className={"header__selection"}>
                        selected {checkedProductsCount} out of {numberOfProducts} products
                    </span>
                    <div className="header__content">
                        <button className="header__selectAll" onClick={clearSelectedProducts}>Clear</button>
                    </div>
                    {/* <Button content={"Clear"} /> */}
                </div>
                <div className={"header__content"}>
                    <Search query={query} changeQuery={changeQuery} getFinalQuery={getFinalQuery} />
                    <Button content={"add to inventory"} />
                    <i class="far fa-question-circle header__questionmark"></i>
                </div>
            </header>
            <Sort onChange={onChange} />
        </>
    );
};

export default Header;