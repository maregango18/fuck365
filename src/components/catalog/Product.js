import React from 'react';
import './Catalog.css';

const Product = ({ product, checkboxChanged, checkedProducts, chooseProduct }) => {
    return (
        <div className="catalog__product" >
            <input className="checkbox__btn" type="checkbox" onChange={e => checkboxChanged(e, product)} checked={checkedProducts.includes(product)}></input>
            <div className="catalog__title"> {product.title}</div>
            <div class="catalog__photo" onClick={() => chooseProduct(product)}>
                <img src={product.image} alt=" " />
            </div>
            <div class="catalog__prices"> {product.price}$ </div>
        </div>
    );
}

export default Product;