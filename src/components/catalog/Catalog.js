import React, { useEffect, useState } from 'react';
import './Catalog.css';
import Product from "./Product";

const Catalog = ({ products , checkboxChanged , checkedProducts , chooseProduct }) => {

    // const [products, setProducts] = useState([]);

    // useEffect(() => {
    //     getProducts();
    // }, []);

    // const getProducts = async () => {
    //     const response = await fetch(
    //         "https://fakestoreapi.com/products"
    //     );
    //     const data = await response.json();
    //     setProducts(data);
    //     console.log(data);
    // };

    return (
        <div className="catalog">
            {products.map(product => (
                <Product product={product} chooseProduct={chooseProduct} checkboxChanged={checkboxChanged} checkedProducts={checkedProducts}/>
            ))}
        </div>
    );
};

export default Catalog;
