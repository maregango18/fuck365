import React from 'react';
import './Aside.css';

function Aside() {
    return (
        <div class="sidenav">
            <select className="sorting__dark">
                <option value="de">Choose Niche</option>
                <option value="as">Best Sellers</option>
                <option value="de">Fashion</option>
            </select>
            <select className="sorting__light">
                <option value="de">Choose Category</option>
                <option value="as">Best Sellers</option>
                <option value="de">Fashion</option>
            </select>
            <select className="sorting">
                <option value="de">Ship From</option>
                <option value="as">Best Sellers</option>
                <option value="de">Fashion</option>
            </select>
            <select className="sorting">
                <option value="de">Ship To</option>
                <option value="as">Best Sellers</option>
                <option value="de">Fashion</option>
            </select>
            <select className="sorting">
                <option value="de">Select Supplier</option>
                <option value="as">Best Sellers</option>
                <option value="de">Fashion</option>
            </select>

        </div>
    );
}
export default Aside;